import { SEARCH_LOAD, SEARCH_UNLOAD } from '../actions';
import { fetchSearch } from '../api/search';

export function loadSearch() {
  return dispatch =>
  fetchSearch()
      .then(payload => {
        dispatch({ type: SEARCH_LOAD, payload: payload.response });
      })
      .catch(payload => {
        console.log('payloadpayloadpayload', payload)
        // alert('something error reLoad page');
      });
}
