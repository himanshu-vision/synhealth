import { POPULARS_LOAD, POPULARS_UNLOAD, POPULAR_LOAD, POPULAR_UNLOAD } from '../actions';
import {
  fetchPopular, 
} from '../api/popularPackage';

export function loadPackages() {
  
  return dispatch => (
  fetchPopular().then((payload)=>{
    let payloads = JSON.parse(payload);
    dispatch({ type: POPULARS_LOAD, payload: payloads.response })
    
  }).catch(payload => {
  })
);
  //return dispatch => (
  //   fetchPopular()
  //     .on('success',
  //       payload => dispatch({ type: TASKS_LOAD, payload })
  //     )
  //     .on('error',
  //       payload => dispatch({ type: TASKS_LOAD, error: true, payload })
  //     )
  //     .start()
  // );
}

// export function unloadPackages() {
//   unwatchTasks();
//   return { type: TASKS_UNLOAD };
// }

export function loadPackage(id) {
  return dispatch => (
    watchTask(id)
      .on('success',
        payload => dispatch({ type: TASK_LOAD, payload })
      )
      .on('error',
        payload => dispatch({ type: TASK_LOAD, error: true, payload })
      )
      .start()
  );
}

export function unloadTask(id) {
  unwatchTask(id);
  return { type: TASK_UNLOAD };
}
