// See https://github.com/acdlite/flux-standard-action

// Session
export const SESSION_LOAD = 'SESSION_LOAD';
export const SESSION_LOGIN = 'SESSION_LOGIN';
export const SESSION_LOGOUT = 'SESSION_LOGOUT';

// Dashboard
export const DASHBOARD_LOAD = 'DASHBOARD_LOAD';
export const DASHBOARD_UNLOAD = 'DASHBOARD_UNLOAD';

// Tasks
export const TASKS_LOAD = 'TASKS_LOAD';
export const TASKS_UNLOAD = 'TASKS_UNLOAD';
export const TASK_LOAD = 'TASK_LOAD';
export const TASK_UNLOAD = 'TASK_UNLOAD';

// Tasks
export const FAQS_LOAD = 'FAQS_LOAD';
export const FAQS_UNLOAD = 'FAQS_UNLOAD';
export const FAQ_LOAD = 'FAQ_LOAD';
export const FAQ_UNLOAD = 'FAQ_UNLOAD';


// USER
export const USER_LOAD = 'USER_LOAD';
export const USER_UNLOAD = 'USER_UNLOAD';

// CART
export const CART_LOAD = 'CART_LOAD';
export const CART_UNLOAD = 'CART_UNLOAD';

// BOOKING
export const BOOKING_LOAD = 'BOOKING_LOAD';
export const BOOKING_UNLOAD = 'BOOKING_UNLOAD';

// SEARCH
export const SEARCH_LOAD = 'SEARCH_LOAD';
export const SEARCH_UNLOAD = 'SEARCH_UNLOAD';

// pupular package
export const POPULARS_LOAD = 'POPULARS_LOAD';
export const POPULARS_UNLOAD = 'POPULARS_UNLOAD';
export const POPULAR_LOAD = 'POPULAR_LOAD';
export const POPULAR_UNLOAD = 'POPULAR_UNLOAD';

// Nav
export const NAV_ACTIVATE = 'NAV_ACTIVATE';
export const NAV_ENABLE = 'NAV_ENABLE';
export const NAV_RESPONSIVE = 'NAV_RESPONSIVE';

//Home
export const HOME_PACKAGE = 'HOME_PACKAGE';
export const MOBILE_LOGIN = 'MOBILE_LOGIN';

