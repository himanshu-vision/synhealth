import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Split from 'grommet/components/Split';
import Sidebar from 'grommet/components/Sidebar';
import Box from 'grommet/components/Box';
import Header from 'grommet/components/Header';
import Title from 'grommet/components/Title';
import FormFields from 'grommet/components/FormFields';
import NumberInput from 'grommet/components/NumberInput';
import Button from 'grommet/components/Button';
import Select from 'grommet/components/Select';
import Add from 'grommet/components/icons/base/Add';
import FormPrevious from 'grommet/components/icons/base/FormPrevious';
import Close from 'grommet/components/icons/base/Close';
import CheckBox from 'grommet/components/CheckBox';
import LoginForm from 'grommet/components/LoginForm';
import FormField from 'grommet/components/FormField';
import TextInput from 'grommet/components/TextInput';
import Article from 'grommet/components/Article';
import Section from 'grommet/components/Section';
import Heading from 'grommet/components/Heading';
import Paragraph from 'grommet/components/Paragraph';
import Logo from 'grommet/components/icons/Grommet';
import Footers from '../components/Footer';
import Footer from 'grommet/components/Footer';
import NavControl from '../components/NavControl';
import Form from 'grommet/components/Form';
import { login } from '../actions/session';
import { navEnable } from '../actions/nav';
import { pageLoaded } from './utils';
import Layer from 'grommet/components/Layer';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';
import RadioButton from 'grommet/components/RadioButton';
import DateTime from 'grommet/components/DateTime';
import PaymentMode from '../components/PaymentMode';
import MrpTotalFinal from '../components/MrpTotalFinal';
import CheckPage from '../components/CheckOut';
import Status from 'grommet/components/icons/Status';
//import Form from 'grommet/components/Form';

class CheckOut extends Component {
  constructor() {
    super();
    this.state = {
      shedule: false,
    };
    this._onClick = this._onClick.bind(this);
    this._onLayerClose = this._onLayerClose.bind(this);
  }
  _onClick() {
    this.setState({ shedule: true });
  }
  _onLayerClose() {
    this.setState({ shedule: false });
  }

  render() {
    return (
      <Article>
        <NavControl />

        <Box
          direction={'row'}
          justify="start"
          align="start"
          wrap={true}
          pad="medium"
          margin="small"
          colorIndex="light-1"
        >
          <Box
            justify="start"
            align="start"
            wrap={true}
            size={'xxlarge'}
            pad="none"
            margin="small"
            colorIndex="light-1"
          >
            <PaymentMode />
          </Box>
          <Box
            justify="start"
            align="start"
            wrap={true}
            margin="large"
            size={'large'}
          >
            <MrpTotalFinal />
            <Button
              size={'large'}
              icon={<Status value="ok" />}
              label="Complete Order"
              onClick={this._onClick}
              href="#"
              primary
            />
          </Box>
        </Box>
        <Footers />
      </Article>
    );
  }
}

export default CheckOut;
