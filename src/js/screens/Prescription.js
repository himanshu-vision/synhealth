import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Split from 'grommet/components/Split';
import Sidebar from 'grommet/components/Sidebar';
import Box from 'grommet/components/Box';
import Header from 'grommet/components/Header';
import FormFields from 'grommet/components/FormFields';
import Button from 'grommet/components/Button';
import Select from 'grommet/components/Select';

import LoginForm from 'grommet/components/LoginForm';
import FormField from 'grommet/components/FormField';
import TextInput from 'grommet/components/TextInput';
import Article from 'grommet/components/Article';
import Section from 'grommet/components/Section';
import Heading from 'grommet/components/Heading';
import Paragraph from 'grommet/components/Paragraph';
import Logo from 'grommet/components/icons/Grommet';
import Footers from '../components/Footer';
import Footer from 'grommet/components/Footer';
import NavControl from '../components/NavControl';
import Form from 'grommet/components/Form';
import { login } from '../actions/session';
import { navEnable } from '../actions/nav';
import { pageLoaded } from './utils';

class Prescription extends Component {
  constructor() {
    super();
    // this._onSubmit = this._onSubmit.bind(this);
  }

  render() {
    // const { session: { error } } = this.props;

    return (
      <Article>
        <NavControl />
        <Box
          justify="center"
          align="center"
          wrap={true}
          pad="medium"
          margin="small"
          colorIndex="light-1"
        >
          <Form>
            <Header>
              <Heading>Contact Us</Heading>
            </Header>
            <FormField label="Full Name" error="">
              <TextInput />
            </FormField>

            <FormField label="Mobile No." error="">
              <TextInput />
            </FormField>

            <FormField label="Email Id" error="">
              <TextInput />
            </FormField>

            <FormField label="Company" error="">
              <TextInput />
            </FormField>

            <FormField label="Message" error="">
              <TextInput />
            </FormField>
            <Footer pad={{ vertical: 'medium' }}>
              <Button
                label="Submit"
                type="submit"
                primary={true}
                //onClick={...}
              />
            </Footer>
          </Form>
        </Box>
        <Footers />
      </Article>
    );
  }
}

export default Prescription;
