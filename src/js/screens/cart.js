import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Split from 'grommet/components/Split';
import Sidebar from 'grommet/components/Sidebar';
import Box from 'grommet/components/Box';
import Header from 'grommet/components/Header';
import Title from 'grommet/components/Title';
import FormFields from 'grommet/components/FormFields';
import NumberInput from 'grommet/components/NumberInput';
import Button from 'grommet/components/Button';
import Select from 'grommet/components/Select';
import Add from 'grommet/components/icons/base/Add';
import FormPrevious from 'grommet/components/icons/base/FormPrevious';
import Close from 'grommet/components/icons/base/Close';
import CheckBox from 'grommet/components/CheckBox';
import LoginForm from 'grommet/components/LoginForm';
import FormField from 'grommet/components/FormField';
import TextInput from 'grommet/components/TextInput';
import Article from 'grommet/components/Article';
import Section from 'grommet/components/Section';
import Heading from 'grommet/components/Heading';
import Paragraph from 'grommet/components/Paragraph';
import Logo from 'grommet/components/icons/Grommet';
import Footers from '../components/Footer';
import Footer from 'grommet/components/Footer';
import NavControl from '../components/NavControl';
import Form from 'grommet/components/Form';
import { login } from '../actions/session';
import { navEnable } from '../actions/nav';
import { pageLoaded } from './utils';
import Layer from 'grommet/components/Layer';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';
import RadioButton from 'grommet/components/RadioButton';
import DateTime from 'grommet/components/DateTime';
import OrderOverView from '../components/OrderOverView';
import MrpTotal from '../components/MrpTotal';
import CheckOut from '../components/CheckOut';
//import Form from 'grommet/components/Form';

class Cart extends Component {
  constructor() {
    super();
    this.state = {
      shedule: false,
    };
    this._onClick = this._onClick.bind(this);
    this._onLayerClose = this._onLayerClose.bind(this);
  }
  _onClick() {
    this.setState({ shedule: true });
  }
  _onLayerClose() {
    this.setState({ shedule: false });
  }

  render() {
    const shedule = this.state.shedule ? (
      <div className="share-layer">
        <Layer
          onClose={this._onLayerClose}
          //closer={true}
          overlayClose={true}
          flush={true}
          align={'right'}
          colorIndex="grey-2"
          // pad={'large'}
        >
          <Header size="medium" colorIndex={'neutral-1-a'}>
            <Button
              icon={<FormPrevious />}
              //onClick={...}
              href="#"
            />
            <Title>Patient Details</Title>
            <Box flex={true} justify="end" direction="row" responsive={false}>
              <Button
                icon={<Close />}
                //onClick={...}
                href="#"
              />
            </Box>
          </Header>
          <Box responsive={true} size={'large'} pad={'medium'}>
            <List>
              <ListItem justify="between" separator="horizontal">
                <CheckBox
                  //label="Sample label"
                  toggle={false}
                  reverse={false}
                  disabled={false}
                />
                <div>
                  <span>Ankit Khandelwal</span>
                  <br />
                  <span>22 male</span>{' '}
                </div>
              </ListItem>
            </List>
            <Button
              icon={<Add />}
              label="Add New Patiennt"
              onClick={this._onClick}
              href="#"
            />
            <Form>
              <Header>
                <Heading strong={true} tag="h4">
                  Step 1: Select Patiennt
                </Heading>
              </Header>
              <FormField label="Name">
                <TextInput />
              </FormField>
              <FormField label="Age">
                <NumberInput
                  value={10}
                  //onChange={...}
                />
              </FormField>
              <FormField label="Gender">
                <RadioButton
                  id="Male"
                  name="Male"
                  label="Male"
                  checked={true}
                  //onChange={...}
                />
                <RadioButton
                  id="Male"
                  name="Female"
                  label="Female"
                  checked={false}
                  //onChange={...}
                />
              </FormField>
              <Footer pad={{ vertical: 'medium' }}>
                <Button
                  label="Cencel"
                  type="submit"
                  primary={true}
                  // onClick={...}
                />
                <Button
                  label="Submit"
                  type="submit"
                  primary={true}
                  // onClick={...}
                />
              </Footer>
            </Form>

            <Form>
              <Header>
                <Heading strong={true} tag="h4">
                  Step 2: Select Address
                </Heading>
              </Header>
              <Button
                icon={<Add />}
                label="Add Address"
                onClick={this._onClick}
                href="#"
              />
              <FormField label="Flat Number, building Name, Street*">
                <TextInput />
              </FormField>
              <FormField label="Pincode">
                <TextInput />
              </FormField>
              <FormField label="Locality">
                <TextInput />
              </FormField>
              <FormField label="LandMark">
                <TextInput />
              </FormField>
              <FormField label="City">
                <TextInput />
              </FormField>
              <FormField label="State">
                <TextInput />
              </FormField>
              <FormField label="10 digit Mobile Number*">
                <TextInput />
              </FormField>
              <Footer pad={{ vertical: 'medium' }}>
                <Button
                  label="Cencel"
                  type="submit"
                  primary={false}
                  // onClick={...}
                />
                <Button
                  label="Submit"
                  type="submit"
                  primary={true}
                  // onClick={...}
                />
              </Footer>
            </Form>

            <Form>
              <FormField>
                <DateTime
                  id="id"
                  name="name"
                  format='YYYY/D/M h'
                  //onChange={...}
                />
              </FormField>
            </Form>
          </Box>
        </Layer>
      </div>
    ) : (
      undefined
    );

    return (
      <Article>
        <NavControl />
        {shedule}
        <Box
          direction={'row'}
          justify="start"
          align="start"
          wrap={true}
          pad="medium"
          margin="small"
          colorIndex="light-1"
        >

<Box
          justify="start"
          align="start"
          wrap={true}
          size={'xxlarge'}
          pad="none"
          margin="small"
          colorIndex="light-1"
        >
        <OrderOverView />
         
          </Box>
          <Box
          justify="start"
          align="start"
          wrap={true}
          margin="large"
          size={'large'}
         
        >
        <MrpTotal />
        <CheckOut />
        </Box>
        </Box>
        <Footers />
      </Article>
    );
  }
}

export default Cart;
