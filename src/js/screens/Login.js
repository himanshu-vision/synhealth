import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Split from 'grommet/components/Split';
import Sidebar from 'grommet/components/Sidebar';
import LoginForm from 'grommet/components/LoginForm';
import Article from 'grommet/components/Article';
import Section from 'grommet/components/Section';
import Heading from 'grommet/components/Heading';
import Paragraph from 'grommet/components/Paragraph';
import Footer from 'grommet/components/Footer';
import Logo from 'grommet/components/icons/Grommet';

import { login } from '../actions/session';
import { navEnable } from '../actions/nav';
import { pageLoaded } from './utils';

class Login extends Component {
  constructor() {
    super();
    this._onSubmit = this._onSubmit.bind(this);
  }

  componentDidMount() {
    pageLoaded('Login');
    this.props.dispatch(navEnable(false));
  }

  componentWillUnmount() {
    this.props.dispatch(navEnable(true));
  }

  _onSubmit(fields) {
    const { dispatch } = this.props;
    const { router } = this.context;
    fields.username = fields.username.substring(3, 13);
    dispatch(
      login(fields.username, fields.password, () =>
        router.history.push('/dashboard')
      )
    );
  }

  render() {
    const { session: { error } } = this.props;

    return (
      <Article>
        <LoginForm
          align="start"
          logo={<Logo className="logo" colorIndex="brand" />}
          title="Synhealth"
          onSubmit={this._onSubmit}
          errors={[error]}
          usernameType="text"
        />
      </Article>
    );
  }
}

Login.defaultProps = {
  session: {
    error: undefined,
  },
};

Login.propTypes = {
  dispatch: PropTypes.func.isRequired,
  session: PropTypes.shape({
    error: PropTypes.string,
  }),
};

Login.contextTypes = {
  router: PropTypes.object.isRequired,
};

const select = state => ({
  session: state.session,
});

export default connect(select)(Login);
