import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';
import Box from 'grommet/components/Box';
import Header from 'grommet/components/Header';
import Heading from 'grommet/components/Heading';
import Label from 'grommet/components/Label';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';
import Notification from 'grommet/components/Notification';
import Paragraph from 'grommet/components/Paragraph';
import Value from 'grommet/components/Value';
import Meter from 'grommet/components/Meter';
import Spinning from 'grommet/components/icons/Spinning';
import { getMessage } from 'grommet/utils/Intl';
import NavControl from '../components/NavControl';
import { loadDashboard, unloadDashboard } from '../actions/dashboard';
import Footer from '../components/Footer';
import Tabs from 'grommet/components/Tabs';
import Tab from 'grommet/components/Tab';
import { pageLoaded } from './utils';
import Image from 'grommet/components/Image';
import Table from 'grommet/components/Table';
import TableRow from 'grommet/components/TableRow';
import { loadUser } from '../actions/user';
import { loadMyBooking } from '../actions/booking';
import { loadCart } from '../actions/cart';

import Section from 'grommet/components/Section';

import {
  fetchUserData,
  updateUserData,
  FetchUserBookings,
} from '../api/userDetails';
import Title from 'grommet/components/Title';
import TextInput from 'grommet/components/TextInput';
import Select from 'grommet/components/Select';
import NumberInput from 'grommet/components/NumberInput';
import RadioButton from 'grommet/components/RadioButton';
import Button from 'grommet/components/Button';

class DashboardUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: {},
      isEditProfile: false,
      newFormData: {},
      userBookings: [],
    };
    this.fetchUserDetails = this.fetchUserDetails.bind(this);
  }
  componentDidMount() {
    //pageLoaded('Tasks');
    //loadPackages();
    this.props.dispatch(loadUser());
    //this.props.dispatch(loadMyBooking());
    //this.props.dispatch(loadCart());
    this.fetchUserDetails();
    this.fetchUserBookings();
  }

  // async fetchUserDetails() {
  //   const result = await fetchUserData();
  //   console.log(result);
  //   if (result.success) {
  //     this.setState({
  //       userData: result.response,
  //       newFormData: result.response,
  //     });
  //   }
  // }
  fetchUserDetails() {
    fetchUserData().then(result => {
      if (result.success) {
        this.setState({
          userData: result.response,
          newFormData: result.response,
        });
      }
    });
  }

  // async fetchUserBookings() {
  //   const result = await FetchUserBookings();
  //   if (result.success) {
  //     this.setState({
  //       userBookings: result.success,
  //     });
  //   }
  // }
  fetchUserBookings() {
    FetchUserBookings().then(result => {
      if (result.success) {
        this.setState({
          userBookings: result.success,
        });
      }
    });
  }

  // async updateUserDetails() {
  //   const { newFormData } = this.state;
  //   const result = await updateUserData(newFormData);
  //   console.log(result);
  //   if (result.success) {
  //   }
  // }

  updateUserDetails() {
    const { newFormData } = this.state;
    updateUserData(newFormData).then(result => {
      if (result.success) {
      }
    });
  }

  RenderEditUserDetails(userData) {
    return (
      <List style={{ display: 'flex', flex: 1, flexDirection: 'column' }}>
        <ListItem justify="between" separator="horizontal">
          <span>Name</span>
          <span className="secondary">
            <TextInput
              id="item1"
              name="item-1"
              value={userData.first_name}
              onDOMChange={dom => {
                this.setState({
                  newFormData: {
                    ...this.state.newFormData,
                    first_name: dom.target.value,
                  },
                });
              }}
              placeHolder={'Full name'}
            />
          </span>
        </ListItem>
        <ListItem justify="between">
          <span>email</span>
          <span className="secondary">
            <TextInput
              id="item1"
              name="item-1"
              value={userData.email}
              onDOMChange={dom => {
                this.setState({
                  newFormData: {
                    ...this.state.newFormData,
                    email: dom.target.value,
                  },
                });
              }}
              placeHolder={'Enter email address'}
            />
          </span>
        </ListItem>
        <ListItem justify="between">
          <span>Mobile No</span>
          <span className="secondary">
            <TextInput
              id="item1"
              name="item-1"
              value={userData.mobile}
              onDOMChange={dom => {
                this.setState({
                  newFormData: {
                    ...this.state.newFormData,
                    mobile: dom.target.value,
                  },
                });
              }}
              placeholder={'Enter mobile numer'}
            />
          </span>
        </ListItem>

        <ListItem justify="between">
          <span>Blood Type</span>
          <span className="secondary">
            <Select
              id="item1"
              name="item-1"
              value={userData.blood_group}
              placeholder={'Select blood group'}
              options={[
                { value: 'A+', label: 'A+' },
                { value: 'A-', label: 'A-' },
                { value: 'B+', label: 'B+' },
                { value: 'B-', label: 'B-' },
                { value: 'AB+', label: 'AB+' },
                { value: 'AB-', label: 'AB-' },
                { value: 'O+', label: 'O+' },
                { value: 'O-', label: 'O-' },
              ]}
              onDOMChange={() => {}}
              onSelect={item => {
                this.setState({
                  newFormData: {
                    ...this.state.newFormData,
                    blood_group: item.value,
                  },
                });
              }}
            />
          </span>
        </ListItem>
        <ListItem justify="between">
          <span>Weight (in Kgs)</span>
          <span className="secondary">
            <NumberInput
              value={userData.weight}
              onChange={event => {
                this.setState({
                  newFormData: {
                    ...this.state.newFormData,
                    weight: event.target.value,
                  },
                });
              }}
            />
          </span>
        </ListItem>
        <ListItem justify="between">
          <span>Age</span>
          <TextInput
            id="item1"
            name="item-1"
            value={userData.age}
            onDOMChange={dom => {
              this.setState({
                newFormData: {
                  ...this.state.newFormData,
                  age: dom.target.value,
                },
              });
            }}
            placeholder={'Enter age'}
          />
        </ListItem>
        <ListItem justify="between">
          <span>Gender</span>
          <span className="secondary">
            <RadioButton
              id="choice1-1"
              name="choice1-1"
              label="Male"
              checked={true}
              onChange={() => {}}
            />
            <RadioButton
              id="choice1-2"
              name="choice1-2"
              label="Female"
              checked={false}
              onChange={() => {}}
            />
          </span>
        </ListItem>
        <ListItem justify="between">
          <span>Height</span>
          <span
            className="secondary"
            style={{ display: 'flex', flexDirection: 'row' }}
          >
            <Select
              id="item1"
              name="item-1"
              options={[
                { value: '2', label: '2' },
                { value: '3', label: '3' },
                { value: '4', label: '4' },
                { value: '5', label: '5' },
                { value: '6', label: '6' },
                { value: '7', label: '7' },
                { value: '8', label: '8' },
              ]}
              onSelect={item => {
                this.setState({
                  newFormData: {
                    ...this.state.newFormData,
                    heightFeet: item.value,
                  },
                });
              }}
            />
            <Select
              id="item1"
              name="item-1"
              options={[
                { value: '0', label: '0' },
                { value: '1', label: '1' },
                { value: '2', label: '2' },
                { value: '3', label: '3' },
                { value: '4', label: '4' },
                { value: '5', label: '5' },
                { value: '6', label: '6' },
                { value: '7', label: '7' },
                { value: '8', label: '8' },
                { value: '9', label: '9' },
                { value: '10', label: '10' },
                { value: '11', label: '11' },
              ]}
              onSelect={item => {
                this.setState({
                  newFormData: {
                    ...this.state.newFormData,
                    heightInch: item.value,
                  },
                });
              }}
            />
          </span>
        </ListItem>

        <ListItem justify="between">
          <span>Waist (in inches)</span>
          <span className="secondary">
            <NumberInput
              value={userData.waist}
              onChange={event => {
                this.setState({
                  newFormData: {
                    ...this.state.newFormData,
                    waist: event.target.value,
                  },
                });
              }}
            />
          </span>
        </ListItem>
        <Button
          style={{ width: 150, padding: 10 }}
          fill={false}
          onClick={() => this.updateUserDetails()}
        >
          Submit
        </Button>
      </List>
    );
  }

  RenderUserData(userData) {
    return (
      <List style={{ display: 'flex', flex: 1, flexDirection: 'column' }}>
        <ListItem justify="between" separator="horizontal">
          <span>Name</span>
          <span className="secondary">
            {userData.first_name} {userData.last_name}
          </span>
        </ListItem>
        <ListItem justify="between">
          <span>email</span>
          <span className="secondary">{userData.email}</span>
        </ListItem>
        <ListItem justify="between">
          <span>Mobile No</span>
          <span className="secondary">{userData.mobile}</span>
        </ListItem>

        <ListItem justify="between">
          <span>Blood Type</span>
          <span className="secondary">{userData.blood_group}</span>
        </ListItem>
        <ListItem justify="between">
          <span>Weight (in Kgs)</span>
          <span className="secondary">{userData.weight}kg</span>
        </ListItem>
        <ListItem justify="between">
          <span>Age</span>
          <span className="secondary">{userData.age}</span>
        </ListItem>
        <ListItem justify="between">
          <span>Gender</span>
          <span className="secondary">{userData.gender}</span>
        </ListItem>
        <ListItem justify="between">
          <span>Height</span>
          <span className="secondary">{userData.height}</span>
        </ListItem>

        <ListItem justify="between">
          <span>Waist (in inches)</span>
          <span className="secondary">{userData.waist}</span>
        </ListItem>
      </List>
    );
  }

  RenderUserDetails(userData, newFormData) {
    return (
      <Box pad="medium">
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            padding: 20,
            marginBottom: 50,
          }}
        >
          <Title>User Profile</Title>
          <Button
            style={{ width: 150, padding: 10 }}
            fill={false}
            onClick={() =>
              this.setState({ isEditProfile: !this.state.isEditProfile })
            }
            primary={!this.state.isEditProfile}
            critical={this.state.isEditProfile}
          >
            {this.state.isEditProfile ? 'Discard' : 'Edit Profile'}
          </Button>
        </div>

        <div
          style={{
            display: 'flex',
            flex: 1,
            flexDirection: 'row',
          }}
        >
          <div style={{ display: 'flex', flex: 1 }}>
            <Box pad="medium">
              <Image src="/img/logov.svg" size="thumb" fit={'contain'} />
            </Box>
          </div>
          <div
            style={{
              display: 'flex',
              flex: 2,
            }}
          >
            {this.state.isEditProfile
              ? this.RenderEditUserDetails(newFormData)
              : this.RenderUserData(userData)}
          </div>
        </div>
      </Box>
    );
  }

  RenderMyBooking() {
    return (
      <Box>
        <Table
          scrollable={true}
          selectable="multiple"
          //onSelect={...}
        >
          <thead>
            <tr>
              <th>Booking Id</th>
              <th>Booking Date</th>

              <th>Status</th>
              <th />
            </tr>
          </thead>
          <tbody>
            <TableRow>
              <td>1</td>
              <td>Alan</td>
              <td className="secondary">plays accordion</td>
            </TableRow>
            <TableRow>
              <td>2</td>
              <td>Chris</td>
              <td className="secondary">drops the mic</td>
            </TableRow>
            <TableRow>
              <td>3</td>
              <td>Eric</td>
              <td className="secondary">rides a bike</td>
            </TableRow>
            <TableRow>
              <td>4</td>
              <td>Tracy</td>
              <td className="secondary">travels the world</td>
            </TableRow>
          </tbody>
        </Table>
      </Box>
    );
  }

  RenderMyReports() {
    return (
      <Box>
        <Table
          scrollable={true}
          selectable="multiple"
          //onSelect={...}
        >
          <thead>
            <tr>
              <th>Booking Id</th>
              <th>Name</th>
              <th>Booking Date</th>
              <th>Test/Package Name</th>
              <th>Download Report</th>
            </tr>
          </thead>
          <tbody>
            <TableRow>
              <td>1</td>
              <td>Alan</td>
              <td className="secondary">plays accordion</td>
            </TableRow>
            <TableRow>
              <td>2</td>
              <td>Chris</td>
              <td className="secondary">drops the mic</td>
            </TableRow>
            <TableRow>
              <td>3</td>
              <td>Eric</td>
              <td className="secondary">rides a bike</td>
            </TableRow>
            <TableRow>
              <td>4</td>
              <td>Tracy</td>
              <td className="secondary">travels the world</td>
            </TableRow>
          </tbody>
        </Table>
      </Box>
    );
  }

  render() {
    const { error, tasks } = this.props;
    const { intl } = this.context;
    const { userData, newFormData } = this.state;
    console.log(newFormData);
    return (
      <Article primary={true}>
        <NavControl />

        <Box pad="medium">
          <Tabs justify="start">
            <Tab title="Dashboard">
              {this.RenderUserDetails(userData, newFormData)}
            </Tab>
            <Tab title="My Booking">{this.RenderMyBooking()}</Tab>
            <Tab title="My Report">{this.RenderMyReports()}</Tab>
          </Tabs>
        </Box>
        <Footer />
      </Article>
    );
  }
}

DashboardUser.defaultProps = {
  error: undefined,
  user: {},
};

DashboardUser.propTypes = {
  dispatch: PropTypes.func.isRequired,
  error: PropTypes.object,
  tasks: PropTypes.arrayOf(PropTypes.object),
};

DashboardUser.contextTypes = {
  intl: PropTypes.object,
};

const select = state => ({ ...state.user });

export default connect(select)(DashboardUser);
