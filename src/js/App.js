import React, { Component } from 'react';
import { IntlProvider, addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import { getCurrentLocale, getLocaleData } from 'grommet/utils/Locale';
import { Provider } from 'react-redux';
import { initialize } from './actions/session';
import store from './store';
import Main from './components/Main';
import Responsive from 'grommet/utils/Responsive';

const locale = getCurrentLocale();
addLocaleData(en);
let messages;
try {
  messages = require(`./messages/${locale}`);
} catch (e) {
  messages = require('./messages/en-US');
}
const localeData = getLocaleData(messages, locale);

if (window.location.pathname !== '/login') {
  store.dispatch(initialize(window.location.pathname));
}

class App extends Component {
  constructor(props) {
    super(props);
    this._onResponsive = this._onResponsive.bind(this);
    this.state = {};
  }
  componentDidMount() {
    this._responsive = Responsive.start(this._onResponsive);
  }

  componentWillUnmount() {
    this._responsive.stop();
  }

  _onResponsive(small) {
    this.setState({ small });
  }

  render() {
    return (
      <Provider store={store}>
        <IntlProvider locale={localeData.locale} messages={localeData.messages}>
          <Main />
        </IntlProvider>
      </Provider>
    );
  }
}

export default App;
