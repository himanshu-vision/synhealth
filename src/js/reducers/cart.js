import { CART_LOAD, CART_UNLOAD } from '../actions';
import { createReducer } from './utils';

const initialState = {
  CART: {}
};

const handlers = {
  [CART_LOAD]: (state, action) => { 
    console.log('actionactionactionaction', action)
    return {
    ...state,
    user: action.payload
  }
}
};

export default createReducer(initialState, handlers);
