import { POPULARS_LOAD, POPULARS_UNLOAD, POPULAR_LOAD, POPULAR_UNLOAD } from '../actions';
import { createReducer } from './utils';

const initialState = {
  packages: [],
  package: undefined
};

const handlers = {
  [POPULARS_LOAD]: (state, action) => { return {
    ...state,
    packages: action.payload
  }
}
  
  //{packages: action.payload}
  // {
  //   console.log('actionactionactionactionaction', action.payload)
  //   if (!action.error) {
  //     action.payload.error = undefined;
  //     return action.payload;
  //   }
  //   return { error: action.payload };
  // },
  // [POPULARS_UNLOAD]: () => initialState,
  // [POPULAR_LOAD]: (state, action) => {
  //   if (!action.error) {
  //     action.payload.error = undefined;
  //     return action.payload;
  //   }
  //   return { error: action.payload };
  // },
  // [POPULAR_UNLOAD]: () => initialState
};

export default createReducer(initialState, handlers);
