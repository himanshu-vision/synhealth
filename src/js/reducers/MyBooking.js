import { BOOKING_LOAD, BOOKING_UNLOAD } from '../actions';
import { createReducer } from './utils';

const initialState = {
  booking: [],
  
};

const handlers = {
  [BOOKING_LOAD]: (state, action) => { 
    console.log('actionactionactionaction', action)
    return {
    ...state,
    booking: action.payload
  }
}
  
};

export default createReducer(initialState, handlers);
