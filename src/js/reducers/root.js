import { combineReducers } from 'redux';

import dashboard from './dashboard';
import nav from './nav';
import session from './session';
import tasks from './tasks';
import packages from './popularPackage';
import faq from './faq';
import user from './user';
import booking from './MyBooking';
import cart from './cart';
import search from './search';


export default combineReducers({
  dashboard,
  nav,
  session,
  tasks,
  packages,
  faq,
  user,
  booking,
  cart,
  search
});
