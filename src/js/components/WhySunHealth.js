import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';

import Image from 'grommet/components/Image';
import Title from 'grommet/components/Title';
import Heading from 'grommet/components/Heading';
import Box from 'grommet/components/Box';

class WhySunHealth extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { error, tasks } = this.props;
    const { intl } = this.context;

    let errorNode;
    let listNode;

    return (
      <Box
        pad={'small'}
        style={{ marginTop: 100, marginBottom: 100, alignItems: 'center' }}
      >
        <Heading
          strong={true}
          truncate={true}
          tag="h2"
          style={{ alingSelf: 'center' }}
        >
          Why SynHealth
        </Heading>
        <Box
          pad={'small'}
          direction={'row'}
          responsive={true}
          style={{
            flex: 1,
            display: 'flex',
            justifyContent: 'space-between',
            marginTop: 100,
          }}
        >
          <CardDesign
            image="../img/package.svg"
            t1={'Customized'}
            t2={'Wide range package'}
          />

          <CardDesign
            image="../img/cost.svg"
            t1={'Cost Effective'}
            t2={'Honest price guaranteed'}
          />

          <CardDesign
            image="../img/convi.svg"
            t1={'Convinient'}
            t2={'Home collection service'}
          />

          <CardDesign
            image="../img/report.svg"
            t1={'Report'}
            t2={'Online reports and analysis'}
          />
        </Box>
      </Box>
    );
  }
}

export default WhySunHealth;

const CardDesign = ({ image, t1, t2 }) => (
  <Box
    style={{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: 50,
      marginRight: 50,
    }}
  >
    <Image
      alt="logo"
      size="small"
      src={image}
      style={{
        width: 120,
        height: 120,
        resizeMode: 'contain',
        marginBottom: 20,
      }}
    />
    <Heading strong={true} truncate={true} tag="h2">
      {t1}
    </Heading>
    <Heading strong={true} truncate={true} tag="h3">
      {t2}
    </Heading>
  </Box>
);
