import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Add from 'grommet/components/icons/base/Add';
import Trash from 'grommet/components/icons/base/Trash';
import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';
import Button from 'grommet/components/Button';
import Image from 'grommet/components/Image';
import Title from 'grommet/components/Title';
import Heading from 'grommet/components/Heading';
import RadioButton from 'grommet/components//RadioButton';
import Box from 'grommet/components/Box';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';
import CheckBox from 'grommet/components/CheckBox';

class OrderOverView extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { error, tasks } = this.props;
    const { intl } = this.context;

    let errorNode;
    let listNode;

    return (
      <Box size={'xxlarge'} colorIndex="light-2">
        <Box pad={{ vertical: 'medium' }}>
          <List>
            <ListItem justify="between" separator="none">
              <Heading strong={true} truncate={true} tag="h2">
                Select Payment Mode
              </Heading>
              <Box className="secondary" />
            </ListItem>
          </List>
        </Box>

        <List selectable={true} pad={{ horizontal: 'xlarge' }}>
          <ListItem justify="between" separator="horizontal">
            <Box direction={'row'} justify="center" align="center">
              <RadioButton
                id="choice1-1"
                name="choice1-1"
                // label='Choice 1'
                checked={true}
                // onChange={...}
              />
              <Box direction={'column'} justify="start" align="start">
                <Heading strong={true} truncate={true} tag="h3">
                  Online Payments
                </Heading>
                <Heading truncate={true} tag="h4">
                  Walltets, Net Banking, UPI, Debit/Credit Card
                </Heading>
              </Box>
            </Box>
          </ListItem>

          <ListItem justify="between" separator="horizontal">
            <Box direction={'row'} justify="center" align="center">
              <RadioButton
                id="choice1-1"
                name="choice1-1"
                // label='Choice 1'
                checked={true}
                // onChange={...}
              />
              <Box direction={'column'} justify="start" align="start">
                <Heading strong={true} truncate={true} tag="h3">
                  Cash On Chample Selection
                </Heading>
              </Box>
            </Box>
          </ListItem>
        </List>
        <Box align='center' justify='center' colorIndex="light-1" pad='medium' >
          <CheckBox
            checked
            label="I accept The terms & Conditions, Disclaimer and privecy Policy"
          />
        </Box>
      </Box>
    );
  }
}

export default OrderOverView;
