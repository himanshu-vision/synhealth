import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';
import Paragraph from 'grommet/components/Paragraph';
import Menu from 'grommet/components/Menu';
import SocialFacebook from 'grommet/components/icons/base/SocialFacebook';
import SocialYoutube from 'grommet/components/icons/base/SocialYoutube';
import SocialTwitter from 'grommet/components/icons/base/SocialTwitter';
import SocialLinkedin from 'grommet/components/icons/base/SocialLinkedin';
import SocialInstagram from 'grommet/components/icons/base/SocialInstagram';
import CloudIcon from 'grommet/components/icons/base/Cloud';
import Button from 'grommet/components/Button';
import Footer from 'grommet/components/Footer';
import Image from 'grommet/components/Image';
import Title from 'grommet/components/Title';
import Heading from 'grommet/components/Heading';
import Box from 'grommet/components/Box';

class Footers extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { error, tasks } = this.props;
    const { intl } = this.context;

    let errorNode;
    let listNode;

    return (
      <Footer justify="between" responsive={true} pad={'medium'}>
        <Box direction="row" responsive={false}>
          <Button
            icon={<SocialFacebook />}
            //onClick={...}
            href="#"
          />

          <Button
            icon={<SocialYoutube />}
            //onClick={...}
            href="#"
          />

          <Button
            icon={<SocialTwitter />}
            //onClick={...}
            href="#"
          />

          <Button
            icon={<SocialLinkedin />}
            //onClick={...}
            href="#"
          />

          <Button
            icon={<SocialInstagram />}
            //onClick={...}
            href="#"
          />
        </Box>
        <Box direction="row" align="start">
          <Box direction="row" align="start" responsive={false}>
            <Link to="/terms">
              <Button
                label="Terms"
                //onClick={...}
                href="#"
                plain={true}
              />
            </Link>
            <Link to="/privacy">
              <Button
                label="Privacy"
                //onClick={...}
                href="#"
                plain={true}
              />
            </Link>
          </Box>
          <Paragraph margin="none">© 2018 SynHealth</Paragraph>
        </Box>
      </Footer>
    );
  }
}

export default Footers;
