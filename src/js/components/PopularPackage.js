import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';

import Image from 'grommet/components/Image';
import Title from 'grommet/components/Title';
import Heading from 'grommet/components/Heading';
import Box from 'grommet/components/Box';
import Card from 'grommet/components/Card';
import Columns from 'grommet/components/Columns';

class PopularPackage extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { error, tasks } = this.props;
    const { intl } = this.context;
    let { packages } = this.props;

    packages = [1, 2, 5, 6];
    return (
      <Box
        pad={'small'}
        style={{ alignItems: 'center', marginTop: 100, marginBottom: 50 }}
      >
        <Heading strong={true} truncate={true} tag="h2">
          Our Most Popular Package
        </Heading>
        <Columns
          pad={'small'}
          direction={'column'}
          responsive={false}
          justify={'start'}
          masonry
          responsive
          size={'small'}
          maxCount={4}
          style={{
            justifyContent: 'space-around',
            marginTop: 100,
          }}
        >
          {packages.map(item => (
            <Card
              style={{ margin: 10, padding: 20 }}
              margin={'small'}
              thumbnail="https://images.pexels.com/photos/131423/pexels-photo-131423.jpeg?h=350&auto=compress&cs=tinysrgb"
              heading={'A1 Diabetic Profile'}
              label={'70 Test'}
              description="Overnight(10-12 hour) fast required"
            />
          ))}
        </Columns>
      </Box>
    );
  }
}

export default PopularPackage;
