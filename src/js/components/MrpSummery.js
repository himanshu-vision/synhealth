import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Add from 'grommet/components/icons/base/Add';
import Trash from 'grommet/components/icons/base/Trash';
import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';
import Button from 'grommet/components/Button';
import Image from 'grommet/components/Image';
import Title from 'grommet/components/Title';
import Heading from 'grommet/components/Heading';
import Box from 'grommet/components/Box';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';
import CheckBox from 'grommet/components/CheckBox';
import TextInput from 'grommet/components/TextInput';
import FormField from 'grommet/components/FormField';

class MrpSummery extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { error, tasks } = this.props;
    const { intl } = this.context;

    let errorNode;
    let listNode;

    return (
      <Box  size={'large'}  colorIndex="light-2" >
        
        <List>
          <ListItem justify="between" separator="none">
            <Heading truncate={true} tag="h4">
              M.R.P. TOTAL
            </Heading>
            <Heading className="secondary" truncate={true} tag="h4">
              300
            </Heading>
          </ListItem>

          <ListItem justify="between" separator="none">
            <Heading truncate={true} tag="h4">
              PRICE DISCOUNT
            </Heading>
            <Heading className="secondary" truncate={true} tag="h4">
              -50
            </Heading>
          </ListItem>

          <ListItem justify="between" separator="none">
            <Heading truncate={true} tag="h4">
              Coupon discount
            </Heading>
            <Heading className="secondary" truncate={true} tag="h4">
              -50
            </Heading>
          </ListItem>

        

          <ListItem justify="between" separator="none">
            <Heading strong={true} truncate={true} tag="h4">
              Total Saving
            </Heading>
            <Heading
              strong={true}
              className="secondary"
              truncate={true}
              tag="h4"
            >
              400
            </Heading>
            
          </ListItem>
          
        </List>
      </Box>
    );
  }
}

export default MrpSummery;
