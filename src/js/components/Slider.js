import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';
import Image from 'grommet/components/Image';
import Box from 'grommet/components/Box';
import Carousel from 'grommet/components/Carousel';

import { pageLoaded } from '../screens/utils';

class Slider extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { error, tasks } = this.props;
    const { intl } = this.context;

    let errorNode;
    let listNode;

    return (
      <Carousel>
        <Image
          fit={'cover'}
          full
          style={styles.imageStyle}
          src="../img/1.png"
        />
        <Image
          fit={'cover'}
          full
          style={styles.imageStyle}
          src="../img/2.jpg"
        />
        <Image
          fit={'cover'}
          full
          style={styles.imageStyle}
          src="../img/3.jpg"
        />
        <Image
          fit={'cover'}
          full
          style={styles.imageStyle}
          src="../img/4.jpg"
        />
        <Image
          fit={'cover'}
          full
          style={styles.imageStyle}
          src="../img/5.jpg"
        />
      </Carousel>
    );
  }
}

export default Slider;

const styles = {
  imageStyle: {
    width: '100%',
    resizeMode: 'contain',
  },
};
