import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Add from 'grommet/components/icons/base/Add';
import Trash from 'grommet/components/icons/base/Trash';
import User from 'grommet/components/icons/base/User';
import Home from 'grommet/components/icons/base/Home';
import Calendar from 'grommet/components/icons/base/Calendar'
import Clock from 'grommet/components/icons/base/Clock';
import Edit from 'grommet/components/icons/base/Edit';
import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';
import Button from 'grommet/components/Button';
import Image from 'grommet/components/Image';
import Title from 'grommet/components/Title';
import Heading from 'grommet/components/Heading';
import Box from 'grommet/components/Box';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';
import CheckBox from 'grommet/components/CheckBox';

class CheckOut extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { error, tasks } = this.props;
    const { intl } = this.context;

    let errorNode;
    let listNode;

    return (
      <Box
        colorIndex="light-2"
        margin={{ vertical: 'medium' }}
        pad={'medium'}
        size={'xxlarge'}
      >
        <Box
          colorIndex="light-1"
          margin={{ vertical: 'medium' }}
          pad={'medium'}
          size={'xxlarge'}
        >
          <List>
            <ListItem justify="between" separator="none">
            <Box direction="row">
            <User />
            <Heading truncate={true} tag="h3">
             Ankit
            </Heading>
          </Box>
            </ListItem>
            <ListItem justify="between" separator="none">
              <Box direction="row">
                <Home />
                <Heading truncate={true} tag="h3">
                 34, dehli-jaipur express way
                </Heading>
              </Box>
            </ListItem>
            <ListItem justify="between" separator="none">
              <Box direction="row">
                <Calendar />
                <Heading truncate={true} tag="h4">
                  March 08, 2018
                </Heading>
              </Box>
            </ListItem>
            <ListItem justify="between" separator="none">
              <Box direction="row">
                <Clock />
                <Heading truncate={true} tag="h4">
                 10:00 AM - 11:00 AM
                </Heading>
              </Box>
            </ListItem>
          </List>
        </Box>
        <Box justify={'center'} align='center' >
        <Button icon={<Edit />} label='Edit'
  //onClick={...}
  href='#'
  primary={true} /></Box>
      </Box>
    );
  }
}

export default CheckOut;
