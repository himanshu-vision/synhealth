import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Box from 'grommet/components/Box';
import Map from 'grommet/components/Map';
import RadioButton from 'grommet/components/RadioButton';

class Tracking extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <Box pad={'medium'} size={'xxlarge'} colorIndex="light-2">
        <Map
          vertical={true}
          data={{
            categories: [
              {
                id: 'category-1',
                label: 'First category',
                items: [
                  {
                    id: 'item-1-1',
                    label: 'First item',
                    node: (
                      <RadioButton
                        id="choice1-1"
                        name="choice1-1"
                        label="Choice 1"
                        checked={true}
                      />
                    ),
                  },
                  {
                    id: 'item-1-2',
                    label: 'Second item',
                    node: (
                      <RadioButton
                        id="choice1-1"
                        name="choice1-1"
                        label="Choice 1"
                        checked={true}
                      />
                    ),
                  },
                  {
                    id: 'item-1-3',
                    label: 'Third item',
                    node: (
                      <RadioButton
                        id="choice1-1"
                        name="choice1-1"
                        label="Choice 1"
                        checked={true}
                      />
                    ),
                  },
                ],
              },
              {
                id: 'category-2',
                label: 'Second category',
                items: [
                  {
                    id: 'item-2-1',
                    label: 'Fourth item',
                    node: (
                      <RadioButton
                        id="choice1-1"
                        name="choice1-1"
                        label="Choice 1"
                        checked={true}
                      />
                    ),
                  },
                  {
                    id: 'item-2-2',
                    label: 'Fifth item',
                    node: (
                      <RadioButton
                        id="choice1-1"
                        name="choice1-1"
                        label="Choice 1"
                        checked={true}
                      />
                    ),
                  },
                ],
              },
              {
                id: 'category-3',
                label: 'Third category',
                items: [
                  {
                    id: 'item-3-1',
                    label: 'Sixth item',
                    node: (
                      <RadioButton
                        id="choice1-1"
                        name="choice1-1"
                        label="Choice 1"
                        checked={true}
                      />
                    ),
                  },
                  {
                    id: 'item-3-2',
                    label: 'Seventh item',
                    node: (
                      <RadioButton
                        id="choice1-1"
                        name="choice1-1"
                        label="Choice 1"
                        checked={true}
                      />
                    ),
                  },
                ],
              },
            ],
            links: [
              { parentId: 'item-1-1', childId: 'item-2-2' },
              { parentId: 'item-1-2', childId: 'item-2-2' },
              { parentId: 'item-1-2', childId: 'item-2-1' },
              { parentId: 'item-2-2', childId: 'item-3-1' },
              { parentId: 'item-2-1', childId: 'item-3-2' },
            ],
          }}
        />
      </Box>
    );
  }
}

export default Tracking;
