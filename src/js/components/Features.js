import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';

import Image from 'grommet/components/Image';
import Title from 'grommet/components/Title';
import Heading from 'grommet/components/Heading';
import Box from 'grommet/components/Box';
import Columns from 'grommet/components/Columns';

class Features extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { error, tasks } = this.props;
    const { intl } = this.context;

    let errorNode;
    let listNode;

    return (
      <Box
        pad={'small'}
        style={{ alignItems: 'center', marginTop: 100, marginBottom: 50 }}
      >
        <Heading strong={false} truncate={true} tag="h1">
          Book Test By Health Condition
        </Heading>
        <Columns
          pad={'small'}
          direction={'column'}
          responsive={false}
          justify={'start'}
          masonry
          responsive
          size={'small'}
          maxCount={4}
          style={{
            justifyContent: 'space-around',
            marginTop: 100,
          }}
        >
          <HealthConditionCard
            text={'Child care'}
            image={'../img/childcare.svg'}
          />
          <HealthConditionCard
            text={'Dibetics'}
            image={'../img/diabetic.svg'}
          />
          <HealthConditionCard
            text={'Vitamins'}
            image={'../img/vitamins.svg'}
          />
          <HealthConditionCard text={'Pregancy'} image={'../img/preg.svg'} />
          <HealthConditionCard text={'Fever'} image={'../img/fever.svg'} />
          <HealthConditionCard text={'Cardic'} image={'../img/cardic.svg'} />
          <HealthConditionCard text={'Strong Bone'} image={'../img/bone.svg'} />
          <HealthConditionCard text={'Allergy'} image={'../img/aller.svg'} />
        </Columns>
      </Box>
    );
  }
}

export default Features;

const HealthConditionCard = ({ image, text }) => (
  <Box
    style={{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      padding: 20,
      margin: 10,
    }}
  >
    <Image
      alt="logo"
      size="small"
      src={image}
      style={{
        marginBottom: 20,
      }}
    />
    <Heading strong={true} truncate={true} tag="h3">
      {text}
    </Heading>
  </Box>
);
