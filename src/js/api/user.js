import RequestWatcher from './request-watcher';
import { headers, parseJSON } from './utils';

export function fetchUser() {
  console.log('headers() ', headers());
  const options = {
    headers: headers(),
    method: 'GET',
    body: {},
    mode: 'no-cors',
  };

  return fetch('https://nextverp.herokuapp.com/customer/update', options).then(
    parseJSON
  );
}
