import RequestWatcher from './request-watcher';
import { headers, parseJSON } from './utils';


export function fetchBooking() {
  console.log('headers() ',headers())
  const options = {
    headers: headers(),
    method: 'GET',
    body: {},
    mode: 'no-cors',
  };

  return fetch('https://nextverp.herokuapp.com/booking/list', options)
    .then(parseJSON);
}

