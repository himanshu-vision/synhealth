import { headers, parseJSON } from './utils';

export function postSession(email, password) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ email, password }),
  };

  return fetch('/api/sessions', options).then(parseJSON);
}

export function loginMobile(mobile) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ mobile }),
    mode: 'no-cors',
  };

  return fetch('https://nextverp.herokuapp.com/otp/generate', options).then(
    parseJSON
  );
}

export function loginOtp(mobile, otp) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ mobile, otp }),
    mode: 'no-cors',
  };

  return fetch('https://nextverp.herokuapp.com/otp/verify', options).then(
    parseJSON
  );
}

export function deleteSession(session) {
  const options = {
    headers: headers(),
    method: 'DELETE',
    mode: 'no-cors',
  };

  return fetch(session.uri, options).then(parseJSON);
}
