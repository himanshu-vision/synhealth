import RequestWatcher from './request-watcher';
import { headers, parseJSON } from './utils';
import GLOBAL from '../constants/global.constants';
import API from '../constants/api.constants';

export function fetchUserData() {
  const options = {
    headers: headers(),
    method: 'GET',
  };
  return fetch(`${GLOBAL.BASE_URL}/${API.CustomerUpdate}`, options).then(
    parseJSON
  );
}

export function updateUserData(body) {
  const customHeader = {
    'Content-Type': '',
  };
  const options = {
    headers: headers(customHeader),
    method: 'PUT',
    body,
  };
  return fetch(`${GLOBAL.BASE_URL}/${API.CustomerUpdate}`, options).then(
    parseJSON
  );
}

export function fetchUserBooking() {
  const options = {
    headers: headers(),
    method: 'GET',
  };
  return fetch(`${GLOBAL.BASE_URL}/${API.CustomerUpdate}`, options).then(
    parseJSON
  );
}
