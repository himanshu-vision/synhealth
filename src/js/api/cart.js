import RequestWatcher from './request-watcher';
import { headers, parseJSON } from './utils';


export function fetchCart() {
  console.log('headers() ',headers())
  const options = {
    headers: headers(),
    method: 'GET',
    body: {},
    mode: 'no-cors',
  };

  return fetch('https://nextverp.herokuapp.com/cart/list', options)
    .then(parseJSON);
}

